###! comparison helpers ###

Handlebars.registerHelper 'ifCond', (v1, operator, v2, options)->
    switch operator
        when '==', '==='
            return if v1 is v2 then options.fn @ else options.inverse @
        when '<'
            return if v1 < v2 then options.fn @ else options.inverse @
        when '<='
            return if v1 <= v2 then options.fn @ else options.inverse @
        when '>'
            return if v1 > v2 then options.fn @ else options.inverse @
        when '>='
            return if v1 >= v2 then options.fn @ else options.inverse @
        when '&&'
            return if v1 && v2 then options.fn @ else options.inverse @
        when '||'
            return if v1 || v2 then options.fn @ else options.inverse @
        else
            return options.inverse @

$(document).foundation()

$(document).scroll( () ->
    scroll_top = $(document).scrollTop()
    if scroll_top <= 300
        $('.floating-return').fadeOut()
    else
        $('.floating-return').fadeIn()
)

$(".floating-return").on "click", ->
    $(document).scrollTop(0)

default_headers = {
    "Accept": "application/json"
    "Authorization": "Token 1ce2d13c0b900d35897fa6bdae992310e3433959"
}

rest_urls = {
    dept_info: (context) -> "http://54.164.38.190:8000/api/group/#{context.code}/.json",
    group_contact_info: (context) -> "http://54.164.38.190:8000/api/emps/group/#{context.code}/.json?#{context.qs}"
}

# fixMenuPadding = () ->
#     # adjust padding on main menu items to fill entire containing block
#     inner = 0
#     outer = $('.main-nav ul').outerWidth()
#     $('.main-nav ul li a').each () ->
#         inner = inner + $(this).outerWidth()
#     pad_factor = (outer-inner)/$('.main-nav ul li a').length/2
#     $('.main-nav ul li a').each () ->
#         padding = parseInt($(this).css('padding-left')) + (pad_factor - 1) + 'px'
#         $(this).css {'padding-left': padding, 'padding-right': padding}

buildRows = (rows) ->
    out = []
    row_init = '<div class="row"></div>'
    group_init = '<div class="medium-6 columns"></div>'
    for row in rows
        row_content = $(row_init)
        for group in row
            group_content = $(group_init)
            for item in group
                group_content.append(item)
            row_content.append(group_content)
        out.push(row_content[0].outerHTML)
    out.join('')

columnize = (context, num_cols=2) ->
    rows = []
    row = []
    group = []
    children = $(context).children()
    $(context).children().each (idx) ->
        self = $(this)
        if self.prop("tagName") is "HR"
            row.push(group)
            group = []
        else
            group.push(self[0].outerHTML)
        
        if (row.length isnt 0 and row.length % num_cols is 0)
            rows.push(row)
            row = []

        if idx + 1 is children.length
            row.push(group)
            rows.push(row)

    context.replaceWith(buildRows(rows))


decipher = (coded) ->
    link = ""
    for i in [0..coded.length] by 2
        chunk = coded[i..i+1]
        link = link + String.fromCharCode(parseInt(chunk, 16))
    return link

unmunge_emails = (selector='a.memail') ->
    $(selector).each((idx, val) ->
        email = decipher($(this).data('coded'))
        $(this).attr('href', 'mailto:' + email)
        empty_span = $(this).children("span:empty")
        if empty_span.length
            empty_span.text(email)
        else
            $(this).text(email)
    )

get_TOC_elements = (selector='h2') ->
    toc_elements = []
    $(selector).each (idx) ->
        self = $(this)
        id = self.attr("id") or "toc#{ idx }"
        title = self.data("short-title") or self.text()
        toc_elements.push([id, title, self.prop("tagName")])
        self.before("<a name=\"#{ id }\"></a>")
        self.attr("data-magellan-destination": id)
    toc_elements

# TODO: Any way to generalize these next three functions a little more?
renderJSONData = (context, data, templ_id, partials='', callbacks='') ->
    # register partials if provided
    if partials
        for partial in partials
            Handlebars.registerPartial(partial, Handlebars.compile($("##{partial}").html()))
    # compile and render the main template
    html_template = $("##{templ_id}").html()
    return if not html_template

    template = Handlebars.compile(html_template)
    rendered = template(data)
    # insert the rendered data into the page
    context.html(rendered)
    # run callbacks if needed
    if callbacks
        callback() for callback in callbacks

renderTemplateFromAJAX = (template, rest_code, target_id, callback) ->
    code = $(".#{target_id}").data("code")
    qs = $(".#{target_id}").data("qs")
    context = { code: code, qs: qs }
    rest_url = rest_urls[rest_code](context)

    ajax_settings = {
        "url": rest_url
        "headers": default_headers
    }
    $.ajax ajax_settings
        .done (data, status) ->
            # console.log "finished rendering dept info"
            if data
                target = $(".#{target_id}")
                if target
                    target.html template(data)
                if callback? and callback
                    callback()

foundationReflow = ->
    $(document).foundation('reflow')

# # Set image loaded callback AFTER Foundation Interchange is triggered
# $(document).on 'replace', '.banner', (e, new_path, original_path) ->
#   video = $('.banner .video video.wait-for-me')
#   Foundation.utils.image_loaded video, ->
#       # calculation to keep image in center of div
#       t = (video.height() - $(".banner .medium-up").height()) / 2
#       video.css({"top": -t})
#       video.get(0).play()

getQS = ->
    queries = {}
    $.each(document.location.search.substr(1).split('&'), (c,q) -> 
        if q
            i = q.split '='
            queries[i[0].toString()] = i[1].toString() 
        )
    queries

$ ->

    # fixMenuPadding()
    # $(window).on 'resize', ->
    #     fixMenuPadding()

    $('.prod.anon #button-open-acad-info-req-form').on 'click', ->
        ga('send', 'event', 'Forms', 'Open', 'Academic request more info')

    renderJSONData($(".do-toc"), {"toc": get_TOC_elements()}, "toc-template", [], [foundationReflow, unmunge_emails])

    $("a.show-search-button").on "click", -> 
        self = $(this)
        $(this).hide 0
        $(".search-button").show 0
        $("input#q").fadeIn 300, ->
            $(this).select()
    
    # add search query back to the search bar after page loads
    $("input#q").val(getQS()?["q"])

    $("img.masked_image").each( ->
        $(this).wrap('<div class="masked_image_wrapper"></div>')
            .parent()
            .append('<div class="image_mask"></div>')
    )

    $(".academic-area-banner .image_mask").each( ->
        $(this).before('<div class="text-mask"></div>')
    )

    # open tagged links in new window
    $("a.new-window").attr("target", "_blank")

    # render departmental data in client-side handlebars template
    if $(".rendered-dept-info").length
        renderTemplateFromAJAX(hbt["templates/partials/academics/dept-info.handlebars"], "dept_info", "rendered-dept-info")
    # render departmental quicklink in client-side handlebars template
    if $(".rendered-dept-quicklink").length
        renderTemplateFromAJAX(hbt["templates/partials/academics/dept-quicklink.handlebars"], "dept_info", "rendered-dept-quicklink")
    # render group contact info in client-side handlebars template
    if $(".rendered-group-contacts").length
        renderTemplateFromAJAX(hbt["templates/partials/contact/group-contacts.handlebars"], "group_contact_info", "rendered-group-contacts", callback=unmunge_emails)

    # add a class for selectively styling page blocks
    # TODO: get this completely into CSS?
    blocks = $(".sidebar .block")
    blocks.each (idx) ->
        is_header_block = $(this).children().length == 1 and $(this).children("h1, h2, h3").filter(":first").length
        if idx+1 isnt blocks.length and not is_header_block
            $(this).addClass("blocky")

    $("input[required]").prev("label").addClass("required")

    # process "more" tags in long lists 
    # HTML looks like this (usually generated from textile):
    # <ul>
    #   <li></li>
    #   ...
    # </ul>
    # <hr class="more">
    # <ul>
    #   <li></li>
    #   ...
    # </ul>
    # The second ul will be replaced with a 'more' button linked
    # to a foundation dropdown
    $("br.more").each (idx)->
        self = $(this)
        id = "drop#{idx}"
        dropdown = $("<a href=\"#\" class=\"more button tiny\" data-dropdown=\"#{id}\">See All &raquo;</a>")
        self.prev("ul").attr("class", "less-list")
        self.next("ul").attr({"id": id, "class": "f-dropdown content more-list", "data-dropdown-content":""})
        self.replaceWith(dropdown)

    $("form #id_address").attr "rows", 4

    $(".csstransforms body").append '<div class="floating-action contact-button show-for-large-up"><a href="/admissions/contact-us/">Request more info</a></div>'

    foundationReflow()
